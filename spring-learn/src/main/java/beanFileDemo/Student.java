package beanFileDemo;

import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.Map;

/**
 * @oauth: qiangSW
 * @date: 2020/2/3 14:56
 * @description: xml
 * @doc:
 */
@Data
//@Component
@ToString
public class Student {

	private Integer id;

	private String name;

	private Integer age;

	private List<String> stringList;

	private Map<String, Object> stringMap;

	public void intiStudent() {
		this.id = 10;
		this.age = 10;
	}

	public void print() {
		System.out.println(10);
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public List<String> getStringList() {
		return stringList;
	}

	public void setStringList(List<String> stringList) {
		this.stringList = stringList;
	}

	public Map<String, Object> getStringMap() {
		return stringMap;
	}

	public void setStringMap(Map<String, Object> stringMap) {
		this.stringMap = stringMap;
	}

	@Override
	public String toString() {
		return "Student{" +
				"id=" + id +
				", name='" + name + '\'' +
				", age=" + age +
				", stringList=" + stringList +
				", stringMap=" + stringMap +
				'}';
	}
}
