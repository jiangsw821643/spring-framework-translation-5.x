package util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @ClassName
 * @Description
 * @Author qiangsw
 * @date 2021/6/21 16:45
 * @Version 1.0
 * TODO
 */
public class SpringContextUtil implements ApplicationContextAware {

	private static ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		SpringContextUtil.applicationContext = applicationContext;
	}

	public static Object getBean(String beanName) {
		return SpringContextUtil.applicationContext.getBean(beanName);
	}

	public static <T> T getBean(String beanName, Class<T> clasz) {

		return SpringContextUtil.applicationContext.getBean(beanName,clasz);
	}
}
