package util;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.context.support.ServletContextResource;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.InputStream;

/**
 * TODO
 *
 * @outhor qiangsw
 * @date 2021/6/24 13:53
 * spring 文件读取
 */
public class ReadXmlFileResourceUtil {


	final static String PATH1 = "context.xml";

	final static String PATH2 = "file:D:\\spring\\Spring-Framework\\spring-learn\\src\\main\\resources\\context.xml";

	final static String PATH3 = "D:\\spring\\Spring-Framework\\spring-learn\\src\\main\\resources\\context.xml";

	public static void main(String[] args) throws Exception {
		ReadXmlFileResourceUtil resourceUtil = new ReadXmlFileResourceUtil();
		InputStream inputStream = resourceUtil.classPathResource().getInputStream();
		int count = 2048;
		byte[] b = new byte[count];
		int readCount = 0;
		StringBuffer buffer = new StringBuffer();
		while (inputStream.read() != -1) {
			inputStream.read(b);
			buffer.append(new String(b));
		}
		System.out.println(new String(buffer));

	}

	/**
	 * 相对路径
	 * 以类路径的方式进行访问
	 *
	 * @return
	 * @throws Exception
	 */
	public Resource classPathResource() throws Exception {
		return new ClassPathResource(ReadXmlFileResourceUtil.PATH1);

	}

	/**
	 * 绝对路径
	 *
	 * @return
	 * @throws Exception
	 */
	public Resource urlResource() throws Exception {
		return new UrlResource(ReadXmlFileResourceUtil.PATH2);
	}

	/**
	 * 绝对路径
	 * 以文件系统绝对路径的方式进行访问
	 *
	 * @return
	 * @throws Exception
	 */
	public Resource fileSystemResource() throws Exception {
		return new FileSystemResource(ReadXmlFileResourceUtil.PATH3);
	}

	/**
	 *以相对于Web应用根目录的方式进行访问
	 * @return
	 * @throws Exception
	 */
	/*public Resource servletContextRescouce() throws Exception {
		return new ServletContextResource();
	}*/
}
