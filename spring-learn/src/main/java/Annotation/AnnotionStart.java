package Annotation;

import beanFileDemo.Student;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @ClassName
 * @Description
 * @Author qiangsw
 * @date 2021/6/2 13:49
 * @Version 1.0
 * TODO
 */
public class AnnotionStart {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(Student.class);
		context.refresh();

		Student bean = context.getBean(Student.class);
		bean.print();
	}
}
