package beanFileDemo;

import lombok.Data;

/**
 * @ClassName
 * @Description
 * @Author qiangsw
 * @date 2021/6/2 14:54
 * @Version 1.0
 * TODO
 */

@Data
public class Grade {

	private Integer id;

	private String subjects;

	private Double score;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSubjects() {
		return subjects;
	}

	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}
}
