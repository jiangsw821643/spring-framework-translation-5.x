package beanFileDemo;

import org.springframework.beans.factory.FactoryBean;

/**
 * TODO
 *
 * @outhor qiangsw
 * @date 2021/6/29 15:04
 * 工厂bean案例使用
 */
public class StudentFactoryBeanDemo implements FactoryBean<Student> {

	@Override
	public Student getObject() throws Exception {
		Student student = new Student();
		student.setAge(1);
		student.setId(2);
		student.setName("studengFactoryBeanDemo");
		return student;
	}

	@Override
	public Class<?> getObjectType() {
		return Student.class;
	}
}
