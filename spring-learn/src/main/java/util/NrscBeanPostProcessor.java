package util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * TODO
 *
 * @outhor qiangsw
 * @date 2021/6/23 13:48
 *  后置处理器
 */
public class NrscBeanPostProcessor implements BeanPostProcessor {


	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("自定义后置处理器(前),bean:"+bean+",beanName:"+beanName);
		return bean;
	}

	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("自定义后置处理器(后),bean:"+bean+",beanName:"+beanName);
		return bean;
	}
}
