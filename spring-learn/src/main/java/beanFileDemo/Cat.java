package beanFileDemo;

import org.springframework.beans.factory.InitializingBean;

/**
 * TODO
 *
 * @outhor qiangsw
 * @date 2021/6/23 11:13
 */
public class Cat implements InitializingBean {
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("cat implements InitializingBean");
	}
}
