package util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

/**
 * TODO
 *
 * @outhor qiangsw
 * @date 2021/6/23 15:00
 *  bean 前置处理器
 */
public class NrsBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	/**
	 * 主要是用来自定义修改持有的bean
	 * ConfigurableListableBeanFactory 其实就是DefaultListableBeanDefinition对象
	 * @param beanFactory the bean factory used by the application context
	 * @throws BeansException
	 */
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		System.out.println("--------------------------------------");
		String[] beanDefinitionNames = beanFactory.getBeanDefinitionNames();
		for(int i=0;i<beanDefinitionNames.length;i++){
			System.out.println("beanName:"+beanDefinitionNames[i]);
		}
		System.out.println("--------------------------------------");
	}
}
