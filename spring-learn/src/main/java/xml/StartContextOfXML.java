package xml;

import beanFileDemo.Cat;
import beanFileDemo.Grade;
import beanFileDemo.Student;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import util.SpringContextUtil;

/**
 * @ClassName
 * @Description
 * @Author qiangsw
 * @date 2021/6/21 15:55
 * @Version 1.0
 * TODO
 */
public class StartContextOfXML<T> {

	/**
	 * 通过扫描xml文件 执行将bean放到spring容器中
	 *
	 * @param args
	 */
	final static String BASE_PATH = "D:\\spring\\Spring-Framework\\";
	//相对路径
	final static String[] XML_PATH0 = {"context.xml"};
	//绝对路径
	final static String[] XML_PATH1 = {"file:" + BASE_PATH + "spring-learn\\src\\main\\resources\\context.xml"};
	//绝对路径
	final static String[] XML_PATH2 = {BASE_PATH + "spring-learn\\src\\main\\resources\\context.xml"};

	/**
	 * 直接使用XmlBeanFactory工厂创建工厂
	 *
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		StartContextOfXML startContextOfXml = new StartContextOfXML();
		startContextOfXml.startContextWay();
	}

	/**
	 * SpringContextUtil
	 */
	public void springContextUtil() {
		ApplicationContext context1 = new ClassPathXmlApplicationContext("classpath:context.xml");
		Student student = SpringContextUtil.getBean("student", Student.class);
		System.out.println(student.toString());
	}


	public void initializingBean() {
		ApplicationContext context1 = new ClassPathXmlApplicationContext("classpath:context.xml");
		Cat cat = SpringContextUtil.getBean("cat", Cat.class);
		//System.out.println(student.toString());
	}

	/**
	 * 创建工厂 DefaultListableBeanFactory
	 * 创建xml读取器 XmlBeanDefinitionReader 读取xml文件,将xml文件中的bean配置转换为 beanDefinition
	 * 读取context.xml配置文件
	 *
	 * @throws Exception
	 */
	public T beanFacotryStartContext(Class<T> clasz) throws Exception {
		//Resource resource = new UrlResource(StartContextOfXML.XML_PATH1[0]);
		Resource resource = new ClassPathResource("context.xml");
		DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
		/*Xml 的beandefinition 读取器*/
		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
		reader.loadBeanDefinitions(resource);
		T bean = beanFactory.getBean(clasz);
		return bean;
		//System.out.println(bean.toString());
	}

	/**
	 * 创建工厂 DefaultListableBeanFactory
	 * 创建xml读取器 XmlBeanDefinitionReader 读取xml文件,将xml文件中的bean配置转换为 beanDefinition
	 * 读取context.xml配置文件
	 *
	 * @throws Exception
	 */
	public Object beanFacotryStartContext(String beanName) throws Exception {
		//Resource resource = new UrlResource(StartContextOfXML.XML_PATH1[0]);
		Resource resource = new ClassPathResource("context.xml");
		DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
		/*Xml 的beandefinition 读取器*/
		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
		reader.loadBeanDefinitions(resource);
		return beanFactory.getBean(beanName);

	}

	/**
	 * 启动容器方式
	 */
	public Object startContextWay() {
		// 用classpath路径 ClassPathXmlApplicationContext
		ApplicationContext context1 = new ClassPathXmlApplicationContext("classpath:context.xml");
		//ApplicationContext context2 = new ClassPathXmlApplicationContext("context.xml");
		//ApplicationContext context3 =
		//		new ClassPathXmlApplicationContext("file:D:\\Spring-Framework\\spring-learning\\src\\main\\resources\\context.xml");
		//这样读取是错误的
		//ApplicationContext context4 = new ClassPathXmlApplicationContext("D:\\Spring-Framework\\spring-learning\\src\\main\\resources\\context.xml");

		//绝对路径扫描 FileSystemXmlApplicationContext
		//ApplicationContext context3 = new FileSystemXmlApplicationContext(xmlPath2);
		//ApplicationContext context3 = new FileSystemXmlApplicationContext("classpath:context.xml");
		//ApplicationContext context5 = new FileSystemXmlApplicationContext("file:D:\\\\Spring-Framework\\\\spring-learning\\\\src\\\\main\\\\resources\\\\context.xml");
		//ApplicationContext context6 = new FileSystemXmlApplicationContext("D:\\Spring-Framework\\spring-learning\\src\\main\\resources\\context.xml");

		//这样读取是错误的
		//ApplicationContext context4 = new FileSystemXmlApplicationContext("context.xml");
		Grade bean = context1.getBean(Grade.class);
		return bean;
		//System.out.println(bean.toString());
	}

	public <T> T startContextWay1(Class<T> clasz){
		ApplicationContext context1 = new ClassPathXmlApplicationContext("classpath:context.xml",StartContextOfXML.class);
		return (T)context1.getBean(clasz.getComponentType());

	}

	public Object registerBean(){
		/*ClassPathXmlApplicationContext context =
				new ClassPathXmlApplicationContext("classpath:context.xml");*/
		Grade grade = new Grade();
		grade.setId(1);
		grade.setSubjects("注册科目");
		GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
		beanDefinition.setBeanClass(grade.getClass());
		AnnotationConfigApplicationContext context1 = new AnnotationConfigApplicationContext();
		context1.registerBeanDefinition("grade",beanDefinition);
		return context1.getBean("grade");
	}
}
